﻿# MisterGames Blueprints v1.0.0

## Usage
- todo

## Assembly definitions
- MisterGames.Blueprints
- MisterGames.Blueprints.Editor

## Dependencies
- MisterGames.Common
- MisterGames.Common.Editor

## Installation 
- Add [MisterGames Common](https://gitlab.com/theverymistergames/common) package
- Top menu MisterGames -> Packages, add packages: 
  - [Blueprints](https://gitlab.com/theverymistergames/blueprints/)