﻿﻿# MisterGames Interact v0.1.0

## Features
- todo

## Assembly definitions
- MisterGames.Interact
- MisterGames.Interact.Editor

## Dependencies
- MisterGames.Common
- MisterGames.Common.Editor
- MisterGames.Input

## Installation
- Add [MisterGames Common](https://gitlab.com/theverymistergames/common/) package
- Top menu MisterGames -> Packages, add packages: 
  - [Input](https://gitlab.com/theverymistergames/input/)
  - [Interact](https://gitlab.com/theverymistergames/interact/)