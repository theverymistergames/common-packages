﻿﻿# MisterGames View v0.1.3

## Features
- Camera controller: provides component to interact with camera (move, rotate, change field of view) from many sources

## Assembly definitions
- MisterGames.View

## Dependencies
- MisterGames.Common

## Installation
- Add [MisterGames Common](https://gitlab.com/theverymistergames/common) package
- Top menu MisterGames -> Packages, add packages: 
  - [View](https://gitlab.com/theverymistergames/view/)