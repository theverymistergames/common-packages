﻿namespace MisterGames.Input.Global {
    
    public enum AxisBinding {
        MouseDelta,
        MouseScroll,
        JoystickStickLeft,
        JoystickStickRight,
    }
    
}